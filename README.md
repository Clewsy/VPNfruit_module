This repository is intended to simplify usage of the wifi pineapple as a VPN gateway.  It consists of two components:
- "autovpn.sh" is a shell script that when copied to "/etc/init.d" can be used to start/stop a defined openvpn config and also enable/disable autostart of the vpn at boot.  I haven't tested it, but I believe this script should work with other openWRT implementations and possibly other distros.
- "VPNfruit" is a module that when copied to "/pineapple/modules/" (local) or "/sd/modules/" (external) provides a gui front-end for controlling the script alongside the pineapple's other modules.

Notes for running on the pineapple:
- Remember to install curl and openvpn
```
$ opkg update
$ opkg install openvpn-openssl
$ opkg install curl
```
- Update the variable in autovpn.sh script to the correct path of your openvpn config.
> OVPN_CONFIG="/root/openvpn_configs/my_ovpn.ovpn"
